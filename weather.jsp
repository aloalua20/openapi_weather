<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@page contentType = "text/html; charset=utf-8"%>
<%@page import="java.sql.*, javax.sql.*, java.io.*, java.net.*"%>
<%@page import="javax.xml.parsers.*, org.w3c.dom.*"%>
<html>
<title>날씨 조회</title>
<head>
<style type = "text/css">
  table {
	width: 100%;
    border-top: 1px solid #444444;
    border-collapse: collapse;
    border: 5px 
  
  }
  th, td {
        border-bottom: 1px solid #444444;
        padding: 10px;
        text-align: center;
  }
	h1 {
	text-align:center;
	}   
</style>

</head><body>
<h1>날씨 조회</h1>
<%
	//48시간 중 순서
	String seq = "";	
	//동네예보 3시간 단위
	String hour = "";		
	//날짜 순서(오늘:0, 내일:1, 모레:2)
	String day = "";	
	//현재 시간 온도
	String temp = "";	
	//최고온도
	String tmx = "";	
	//최저온도
	String tmn = "";	
	//하늘 상태 (1:맑음, 2:구름조금, 3:구름많음, 4:흐림)
	String sky = "";	
	//강수 상태 (0: 없음, 1:비, 2:비/눈, 3:눈/비, 4:눈)
	String pty = "";	
	//날씨(한글)
	String wfKor = "";	
	//날씨 (영어)
	String wfEn = "";	
	//강수 확률
	String pop = "";	
	//12시간 예상 강수량
	String r12 = "";	
	//12시간 예상 적설량
	String s12 = "";
	//풍속(m/s)
	String ws = "";
	//풍향(0:북, 1:북동, 2:동, 3:남동, 4:남, 5:남서, 6:서, 7:북서)
	String wd = "";
	//풍향 (한글)
	String wdKor = "";
	//풍향 (영어)
	String wdEn = "";
	//습도 (%)
	String reh = "";
	// 6시간 예상 강수량
	String r06 = "";
	//6시간 예상 적설량
	String s06 = "";

	//파일에서 직접 파싱, DocumentBuilderFactory, builder 생성
	DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	
	//url의 데이터를 xml형식으로 가져오기
	String urlStr = "http://www.kma.go.kr/wid/queryDFS.jsp?gridx=61&gridy=123"; 
 
	// URL url = new URL(urlStr);
	// HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
 
	// //openStream() : URL페이지 정보를 읽어온다.
	// BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(),"utf-8"));
    
	// String inputLine;
	// String buffer = "";
    
	// // 페이지의 정보를 buffer에 저장한다.
	// while ((inputLine = in.readLine()) != null){
     // buffer += inputLine.trim();
	// }
	
	// InputSource is = new InputSource(new StringReader(buffer));
	// //in.close();
	
	//xml문서를 파싱 > 그 결과를 document 인스턴스에 저장
	Document doc = docBuilder.parse(urlStr);
	//해당 노드에 접근해서 필요 데이터 추출
	// Document의 root node에 접근, 데이터 저장
	Element root = doc.getDocumentElement();
	//xml의 루트를 기준으로 data테그 찾기
	NodeList tag_001 = doc.getElementsByTagName("data");
%>



<table cellspacing=1  border=1>
	<tr >
	<td>seq</td>
	<td>hour</td>
	<td>day</td>
	<td>temp</td>
	<td>tmx</td>
	<td>tmn</td>
	<td>sky</td>
	<td>pty</td>
	<td>wfKor</td>
	<td>wfEn</td>
	<td>pop</td>
	<td>r12</td>
	<td>s12</td>
	<td>ws</td>
	<td>wd</td>
	<td>wdKor</td>
	<td>wdEn</td>
	<td>reh</td>
	<td>r06</td>	
	<td>s06</td>
	</tr>
<%	
	for(int i=0;i<tag_001.getLength(); i++){
		Element elmt=(Element)tag_001.item(i);
		
		//data태그 중 하나를 선택(item(i))하여 attribute가 seq인 것을 찾고 그 값을 찾음	
		seq=tag_001.item(i).getAttributes().getNamedItem("seq").getNodeValue();
		
		//현재 data테그를 기준으로 하위 요소를 찾고 해당 노드값 반환
		hour=elmt.getElementsByTagName("hour").item(0).getFirstChild().getNodeValue();
		day=elmt.getElementsByTagName("day").item(0).getFirstChild().getNodeValue();
		temp=elmt.getElementsByTagName("temp").item(0).getFirstChild().getNodeValue();
		tmx=elmt.getElementsByTagName("tmx").item(0).getFirstChild().getNodeValue();
		tmn=elmt.getElementsByTagName("tmn").item(0).getFirstChild().getNodeValue();
		sky=elmt.getElementsByTagName("sky").item(0).getFirstChild().getNodeValue();
		pty=elmt.getElementsByTagName("pty").item(0).getFirstChild().getNodeValue();
		wfKor=elmt.getElementsByTagName("wfKor").item(0).getFirstChild().getNodeValue();
		wfEn=elmt.getElementsByTagName("wfEn").item(0).getFirstChild().getNodeValue();
		pop=elmt.getElementsByTagName("pop").item(0).getFirstChild().getNodeValue();
		r12=elmt.getElementsByTagName("r12").item(0).getFirstChild().getNodeValue();
		s12=elmt.getElementsByTagName("s12").item(0).getFirstChild().getNodeValue();
		ws=elmt.getElementsByTagName("ws").item(0).getFirstChild().getNodeValue();
		wd=elmt.getElementsByTagName("wd").item(0).getFirstChild().getNodeValue();
		wdKor=elmt.getElementsByTagName("wdKor").item(0).getFirstChild().getNodeValue();
		wdEn=elmt.getElementsByTagName("wdEn").item(0).getFirstChild().getNodeValue();
		reh=elmt.getElementsByTagName("reh").item(0).getFirstChild().getNodeValue();
		r06=elmt.getElementsByTagName("r06").item(0).getFirstChild().getNodeValue();
		s06=elmt.getElementsByTagName("s06").item(0).getFirstChild().getNodeValue();

		//코드에 따른 스트링 값 
		switch(day){
			case "0": day="오늘";
			hour="오늘"+hour+"시";
			break;
			case "1": day="내일";
			hour="내일"+hour+"시";
			break;
			case "2": day="모레";
			hour="모레"+hour+"시";
			break;
		}
		
		switch(sky) {
			case "1": sky="맑음";
			break;
			case "2": sky="구름조금";
			break;
			case "3": sky="구름많음";
			break;
			case "4": sky="흐림";
			break;
		}
		
		switch(pty) {
			case "0": pty="없음";
			break;
			case "1": pty="비";
			break;
			case "2": pty="비/눈";
			break;
			case "3": pty="눈/비";
			break;
			case "4": pty="눈";
			break;
		}
		
		switch(wd){
			case "0": wd="북";
			break;
			case "1": wd="북동";
			break;
			case "2": wd="동";
			break;
			case "3": wd="남동";
			break;
			case "4": wd="남";
			break;
			case "5": wd="남서";
			break;
			case "6": wd="서";
			break;
			case "7": wd="북서";
			break;
		}
%>		
	<tr>
	<!--
	getFirstChild():element node에 접근
	getNodeeValue(): 해당 node의 값 가져오는 메서드
	-->
	<td><%=seq%></td>
	<td><%=hour%></td>
	<td><%=day%></td>
	<td><%=temp%> ºC</td>
	<td><%=tmx%></td>
	<td><%=tmn%></td>
	<td><%=sky%></td>
	<td><%=pty%></td>
	<td><%=wfKor%></td>
	<td><%=wfEn%></td>
	<td><%=pop%></td>
	<td><%=r12%></td>
	<td><%=s12%></td>
	<td><%=ws%></td>
	<td><%=wd%></td>
	<td><%=wdKor%></td>
	<td><%=wdEn%></td>
	<td><%=reh%></td>
	<td><%=r06%></td>
	<td><%=s06%></td>
	</tr>
<%	
	}
	
%>

</body>
</html>